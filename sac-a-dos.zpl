param fichier := "sac-a-dos-24.txt" ;
param capacite := read fichier as "1n" comment "#" use 1 ;
set Objets := { read fichier as "<1s>" comment "#" skip 1 } ;
do print "nb objets : " , card(Objets) ;
param poids[Objets] := read fichier as "<1s> 2n" comment "#" skip 1 ;
param valeurs[Objets] := read fichier as "<1s> 3n" comment "#" skip 1 ;
var x[Objets] binary;
maximize valeur : sum<i> in Objets: valeurs[i] * x[i];
subto poids : sum<i> in Objets: poids[i] * x[i] <= capacite;