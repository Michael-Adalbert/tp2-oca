param C := 9;
param N := 24;
set I := {1 to N by 1};
set J := I;

param tmp[I] := <1> 6, <2> 6, <3> 5, <4> 5, <5> 5, <6> 4, <7> 4, <8> 4, <9> 4, <10> 2, <11> 2, <12> 2, <13> 2, <14> 3, <15> 3, <16> 7, <17> 7, <18> 5, <19> 5, <20> 8, <21> 8, <22> 4, <23> 4, <24> 5 ;
param taille[<i> in I] := tmp[i];

var x[I*J] binary;
var y[J] binary;

minimize nbboites : sum<j> in J : y[j];
subto capacite : forall<j> in J: sum<i> in I: x[i,j] * taille[i] <= C ;
subto occupation : forall<i,j> in I*J: y[j] >= x[i,j] ;
subto tout_pris : forall<i> in I: sum<j> in J: x[i,j] >= 1 ;