param D := 6;
param file := "jobshop.in";
set Machine := { read file as "<2n>" match "s" comment "c" };
param Nb_machine := card(Machine);
set Task := { read file as "<2n>" match "e" comment "c" };
set Mach := { read file as "<3n>" match "e" comment "c" };
param Nb_task := card(Task);
do print "nb machine : ", Nb_machine, " ,nb task :", Nb_task;

param duration[Task*Mach] := read file as "<2n,3n> 4n" comment "c" match "e" ;
param cost[Machine] := read file as "<2n> 3n" comment "c" match "s" ;

var b[Machine * Task] binary;

minimize cost : sum<ma> in Machine: sum<t> in Task : (duration[t,ma]*cost[ma]) * b[ma,t];
subto maxtime : forall<ma> in Machine: sum<t> in Task : duration[t,ma] * b[ma,t] <= D;
subto sur_une_seul : forall<t> in Task:  sum<ma> in Machine: b[ma,t] == 1;