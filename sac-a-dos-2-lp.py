#!/usr/bin/python3
# coding: utf-8
# Début de programme python pour transformer une instance du sac à dos en LP
# 	le fichier peut contenir des lignes de commentaires commençant par #
# 	la première ligne non commentée doit contenir la capacité
# 	les autres lignes doivent décrire les objets, 1 par ligne, d'abord une id puis le poids puis la valeur

fichier_entree = open("sac-a-dos-100.txt", 'r')
fichier_sortie = open("sac-a-dos-100.lp", 'w')

objectif = ""
objectif_sep = ""
contrainte = "c:"
contrainte_sep = ""
fin_contrainte = "<="
variable = ""
deja_capacite = False
capacite = 0
for ligne in fichier_entree:
    toks = ligne.split()
    if len(toks) > 0 and toks[0] != '#':
        if not deja_capacite:
            capacite = toks[0]
            deja_capacite = True
        else:
            variable = variable + "x" + toks[0] + " "
            contrainte = contrainte + contrainte_sep + toks[1] + " x" + toks[0]
            contrainte_sep = " + "
            objectif = objectif + objectif_sep + toks[2] + " x" + toks[0]
            objectif_sep = " + "

contrainte = contrainte + " <= " + str(capacite)

fichier_sortie.write("Maximize\n")
fichier_sortie.write(objectif + "\n")
fichier_sortie.write("Subject To\n")
fichier_sortie.write(contrainte + "\n")
fichier_sortie.write("Binary\n")
fichier_sortie.write(variable)
fichier_sortie.write("End")

fichier_entree.close()
fichier_sortie.close()
