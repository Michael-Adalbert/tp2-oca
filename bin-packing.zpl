##################
#
# Exercice: bin packing en ZIMPL
# récupérés sur :
# https://www.coga.tu-berlin.de/fileadmin/i26/download/AG_DiskAlg/FG_KombOptGraphAlg/bley/CMIP2012/exercise01.tgz
#
# TU Berlin, Prof. Andreas Bley, exercices de MILP, 2012
##################

# version : 20 mars 2020

param fichier := "./u20_00.bpa";
param C := read fichier as "1n" use 1 skip 1;
param N := read fichier as "2n" use 1 skip 1;

set I := {1 to N by 1};
set J := I ; # autant de boites potentielles que d'objets
# Rq : en fait, on n'a pas vraiment besoin de I

# lecture des tailles = pas facile, car le fichier n'a pas de colonne d'index
# => astuce !!
set tmp[<i> in I] := {read fichier as "<1n>" skip 1+i use 1};
param taille[<i> in I] := ord(tmp[i],1,1);

var x[I*J] binary ;
var y[J] binary ;



minimize nbboites : sum<j> in J : y[j] ;
subto capacite : forall<j> in J: sum<i> in I: x[i,j] * taille[i] <= C ;
subto occupation : forall<i,j> in I*J: y[j] >= x[i,j] ;
subto tout_pris : forall<i> in I: sum<j> in J: x[i,j] >= 1 ;